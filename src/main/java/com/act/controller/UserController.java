package com.act.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.act.entity.User;

@RestController
@RequestMapping("/users")
public class UserController {

	@GetMapping("/user")
	@ResponseBody
	public User getUser() {
		return User.builder()
				.uname("Sujith")
				.email("sujith@test.com")
				.city("galle")
				.build();
	}
}
